# kwic

Alunos:
Diogo Queiroz dos Santos - 15/0123418 e
Leonardo de Almeida - 15/0135491

# Instruçes para execução
1. Instalação do ruby versão 2.5.1, para mais detalhes http://rvm.io/
2. Instalação da gema bundler, para baixar todas as dependencias do projeto
3. Executar o comando ''bundle install'' para instalar as dependencias
4. Executar o comando ''ruby main.rb'' para executar o programa
5. Para rodar a suite de testes executar o comando ''rspec''
